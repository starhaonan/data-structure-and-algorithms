package stack;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author: nanlemme
 * @className: LinkedListStackTest
 * @description:
 * @since: jdk11
 */
class LinkedListStackTest {

    @Test
    public void offer() {


    }

    @Test
    public void pop() {
        LinkedListStack<Integer> stack = new LinkedListStack<>(3);
        stack.push(1);
        stack.push(2);
        stack.push(3);
        assertEquals(3,stack.pop());
        assertEquals(2,stack.pop());
        assertEquals(1,stack.pop());
        assertNull(stack.pop());

        //assertEquals(3,stack.peek());
    }

}