package binarytree.binarysearchtree;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author: nanlemme
 * @className: BSTTreeTest
 * @description:
 * @since: jdk11
 */
public class BSTTreeTest {


    BSTNode n1 = new BSTNode(1, "张无忌", null, null);
    BSTNode n3 = new BSTNode(3, "宋青书", null, null);

    BSTNode n5 = new BSTNode(5, "张", null, null);
    BSTNode n7 = new BSTNode(7, "宋", null, null);

    BSTNode n2 = new BSTNode(2, "周", n1, n3);
    BSTNode n6 = new BSTNode(6, "周芷若", n5, n7);
    BSTNode root = new BSTNode(4, "小邵", n2, n6);
    BSTTree tree = new BSTTree(root);
    /*
     *                         4
                             /   \
                            2     6
                           / \   / \
                          1  3  5   7
                            1 2 3 4 5 6 7
     **/
    @Test
    public void get() {
        BSTTree tree = new BSTTree(root);
        assertEquals("小邵", tree.get(4));
        assertEquals("周芷若", tree.get(6));
        assertEquals("周", tree.get(2));
        assertEquals("宋", tree.get(7));
        assertEquals("宋青书", tree.get(3));
    }

    @Test
    public void min() {
       assertEquals("张无忌",tree.min());
    }

    @Test
    public void max() {
        assertEquals("宋",tree.maxRecurision(root));
    }

    @Test
    public void put() {
        tree.put(10,"123");
        assertEquals("123",tree.get(10));
    }

    @Test
    public void  predecessor() {

        assertEquals("小邵",tree.predecessor(2));
    }

    @Test
    public void successor() {
    }

    @Test
    public void delete() {
    }
}