package binarytree.leetcode;

import binarytree.TreeNode;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author: nanlemme
 * @className: LC111Test
 * @description:
 * @since: jdk11
 */
public class LC_111_1Test {
    @Test
    public void test1(){
        TreeNode root = new TreeNode(new TreeNode(2),1,null);

        assertEquals(2,new LC_111_1().minDepth(root));
    }
    @Test
    public void test2(){
        TreeNode root = new TreeNode(1);
        assertEquals(1,new LC_111_1().minDepth(root));
    }


}