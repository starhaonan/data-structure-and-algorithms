package dequeue;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author: nanlemme
 * @className: LinkedListDequeueTest
 * @description:
 * @since: jdk11
 */
class LinkedListDequeueTest {

    @Test
    public void offer(){
        LinkedListDequeue<Integer> dequeue = new LinkedListDequeue<>(4);
        dequeue.offerFirst(1);
        dequeue.offerFirst(2);
        dequeue.offerFirst(3);
        dequeue.offerLast(0);
        assertFalse(dequeue.offerFirst(4));
        assertIterableEquals(List.of(3,2,1,0),dequeue);

    }
    @Test
    public void poll(){
        LinkedListDequeue<Integer> dequeue = new LinkedListDequeue<>(4);
        dequeue.offerFirst(1);
        dequeue.offerFirst(2);
        dequeue.offerFirst(3);
        dequeue.offerLast(0);
        assertEquals(0,dequeue.pollLast());
    }

}