package priorityqueue;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author: nanlemme
 * @className: PriorityQueueTest
 * @description:
 * @since: jdk11
 */
class PriorityQueueTest {

    @Test
    void offer() {
        PriorityQueue<Entry> queue = new PriorityQueue<>(10);
        queue.offer(new Entry("task1",4));
        queue.offer(new Entry("task2",9));
        queue.offer(new Entry("task3",1));
        //assertFalse(queue.offer(new Entry("task3",1)));
        assertEquals(9,queue.peek().priority);

        assertEquals(9,queue.poll().priority());
        assertEquals(4,queue.peek().priority);
        /*assertEquals(9,queue.poll().priority());
        assertEquals(4,queue.poll().priority());
        assertEquals(1,queue.poll().priority());*/
    }

    @Test
    void poll() {
    }
}