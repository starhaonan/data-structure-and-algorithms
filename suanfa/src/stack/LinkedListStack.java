package stack;

import lombok.AllArgsConstructor;
import queue.LinkedListQueue;

import java.util.Iterator;

/**
 * @author: nanlemme
 * @className: LinkedListStack
 * @description: 单向链表实现---> 栈 (先入后出)
 * @since: jdk11
 */
public class LinkedListStack<E> implements Stack<E> {
    @AllArgsConstructor
    static class Node<E> {
        E value; //值
        Node<E> next;
    }

    private int capacity; //栈的容量
    private int size;//节点数(往栈加了几个节点)

    //头指针刚开始指向哨兵节点
    private Node<E> head = new Node<>(null, null);//哨兵节点的value是随意的,刚开始栈为空 哨兵.next = null

    public LinkedListStack(int capacity) {
        this.capacity = capacity;
    }

    /**
     * @description: 向栈顶压入元素
     * @param value 待压入值
     * @return 压入成功返回 true, 压入失败返回 false
     */
    @Override
    public boolean push(E value) {
        if (isFull()){
            return false;
        }
        //栈没满
        //创建一个新节点,然后再把head指向栈顶(栈的特性,栈顶--->想象成一摞书head始终在最上面)
        //  head -> 2 -> 1 -> null  (head节点在最顶部占着)
        Node<E> newNode = new Node<>(value, head.next);//有参构造head.next把2和1连在一块了也就是2.next=1;
        head.next = newNode;                            //这一步把head.next = 2;
        size++;//添加入栈顶成功,节点数量+1
        return true;
    }

    /**
     * @description: 从栈顶弹出元素
     * @return 栈非空返回栈顶元素, 栈为空返回 null
     */
    @Override
    public E pop() {
        if (isEmpty()){
            return null;
        }
        //  head -> 2 -> 1 -> null
        Node<E> first = head.next;//2节点,也就是栈中有效元素的顶部
        head.next = first.next;//把2节点跳过去,直接用head->1,现在这种有效元素的顶部变成1了
        size--;//栈删除成功,节点数-1
        return first.value;
    }

    /**
     * @description: 返回栈顶元素, 不弹出
     * @return 栈非空返回栈顶元素, 栈为空返回 null
     */
    @Override
    public E peek() {
        if (isEmpty()){
            return null;
        }
        //  head -> 2 -> 1 -> null
        return  head.next.value;//返回顶部元素的值
    }

    /**
     * @description: 判断栈是否为空
     * @return 空返回 true, 否则返回 false
     */
    @Override
    public boolean isEmpty() {
        //如果head.next指向的是null说明栈中没有元素,栈为空
        //也可以用size = 0,来表示栈空
        return head.next == null;
    }

    /**
     * @description: 判断栈是否已满
     * @return: 满返回 true, 否则返回 false
     **/
    @Override
    public boolean isFull() {
        return size == capacity;
    }
}
