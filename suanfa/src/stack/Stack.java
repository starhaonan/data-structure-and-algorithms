package stack;

/**
 * @author: nanlemme
 * @className: Stack
 * @description:
 * @since: jdk11
 */
public interface Stack<E> {

    /**
     * @description: 向栈顶压入元素
     * @param value 待压入值
     * @return 压入成功返回 true, 否则返回 false
     */
    boolean push(E value);

    /**
     * @description: 从栈顶弹出元素
     * @return 栈非空返回栈顶元素, 栈为空返回 null
     */
    E pop();

    /**
     * @description: 返回栈顶元素, 不弹出
     * @return 栈非空返回栈顶元素, 栈为空返回 null
     */
    E peek();

    /**
     * @description: 判断栈是否为空
     * @return 空返回 true, 否则返回 false
     */
    boolean isEmpty();

    /**
     * @description: 判断栈是否已满
     * @return: 满返回 true, 否则返回 false
     **/
    boolean isFull();
}