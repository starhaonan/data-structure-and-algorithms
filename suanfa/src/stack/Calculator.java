package stack;

/**
 * @author: Star
 * @className: Calculator
 * @description:
 * @date: 2023/7/18 8:58
 * @version: 1.0
 * @since: jdk11
 */
public class Calculator {
    public static void main(String[] args) {
        String expression = "3+2*6-2";
        //创建两个栈,一个数栈,一个符号栈
        Stack2 numStack = new Stack2(10);
        Stack2 operStack = new Stack2(10);
    }
}


//先创建一个栈
class Stack2 {
    private int maxSize; //栈的大小
    private int[] stack;//数组模拟栈,数据放在数组里面
    private int top = -1;//栈顶,-1表示栈中没数据(也就是数组中)

    public Stack2(int maxSize) {
        this.maxSize = maxSize;
        stack = new int[this.maxSize];
    }

    //栈满
    public boolean isFull() {
        return top == maxSize - 1;
    }

    //栈空
    public boolean isEmpty() {
        return top == -1;
    }

    //入栈-pop
    public void pop(int value) {
        if (isFull()) {
            System.out.println("stack is full");
            return;
        }
        top++;
        stack[top] = value;
    }

    //出栈-push
    public int push() {
        if (isEmpty()) {
            throw new RuntimeException("tack is empty");
        }
        int value = stack[top];
        top--;
        return value;
    }

    //遍历栈,遍历时需要从top开始显示数据
    public void list() {
        if (isEmpty()) {
            System.out.println("stack is empty,don't date");
        }
        for (int i = top; i >= 0; i--) {
            System.out.printf("stack[%d] = %d\n", i, stack[i]);
        }
    }

    /**
     * @description: 返回运算符的优先级
     * @param: 传进来的符号位
     * @return: 返回值越大优先级越高
     */
    public int prority(int oper) {
        if (oper == '*' || oper == '/') {
            return 1;
        } else if (oper == '+' || oper == '-') {
            return 0;
        } else {
            return -1;
        }
    }

    /**
     * @description: 判断是当前指向的元素是不是运算符
     * @param: 当前指向的元素
     * @return: true是运算符, false不是
     **/
    public boolean isOper(char val) {
        return val == '*' || val == '/' || val == '+' || val == '-';
    }

    /**
     * @description: 计算方法
     * @param: [num1, num2, oper]传入两个数字,和一个操作符
     * @return: int  返回的num1和num2计算的值
     **/
    public int cal(int num1, int num2, char oper) {
        int result = 0;//用于存放计算的结果
        switch (oper) {
            case '+':
                result = num1 + num2;
                break;
            case '-':
                result =num2 - num1  ;
                break;
            case '*':
                result = num1 * num2;
                break;
            case '/':
                result = num2 / num1;
                break;
            default:
                System.out.println("数字不合法");
                break;
        }
        return result;
    }
}