package dequeue;

/**
 * @author: nanlemme
 * @className: Deque
 * @description: 双端队列  Deque--->Double ended queue
 * @since: jdk11
 */
public interface Deque<E> {

    /**
     * @description: 向队列头部添加元素
     * @param e 待添加的值
     * @return 添加成功返回 true, 否则返回 false
     */
    boolean offerFirst(E e);

    /**
     * @description: 向队列尾部添加元素
     * @param e 待添加的值
     * @return 添加成功返回 true, 否则返回 false
     */
    boolean offerLast(E e);

    /**
     * @description: 向队列头部删除元素
     * @return: 返回已删除的值
     **/
    E pollFirst();

    /**
     * @description: 向队列尾部删除元素
     * @return: 返回已删除的值
     **/
    E pollLast();

    /**
     * @description: 查看队列头部的元素,不删除
     * @return 返回队列头部元素
     */
    E peekFirst();

    /**
     * @description: 查看队列尾部的元素,不删除
     * @return 返回队列尾部元素
     */
    E peekLast();


    boolean isEmpty();

    boolean isFull();
}
