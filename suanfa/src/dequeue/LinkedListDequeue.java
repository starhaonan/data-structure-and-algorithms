package dequeue;

import java.util.Iterator;

/**
 * @author: nanlemme
 * @className: LinkedListDequeue
 * @description: 双向环形链表实现双端队列
 *              为什么用双向链表,之前实现队列就只用了单向链表?
 *                  因为双端队列,要在头部和尾部删除数据,而在链表中删除元素要知道pre.
 *                  队列只需要在头部删除数据,用哨兵都可以完成而双端队列要在头部和尾部都移除元素
 */
public class LinkedListDequeue<E> implements Deque<E>, Iterable<E> {

    static class Node<E> {
        Node<E> pre;
        E value;
        Node<E> next;

        public Node(Node<E> pre, E value, Node<E> next) {
            this.pre = pre;
            this.value = value;
            this.next = next;
        }
    }

    int capacity; //队列的大小
    int size; //队列中元素个数
    Node<E> sentinel = new Node<>(null, null, null);//哨兵

    public LinkedListDequeue(int capacity) {
        this.capacity = capacity;
        //给哨兵初始化
        sentinel.next = sentinel;
        sentinel.pre = sentinel;
    }

    /**
     * @param e 待添加的值
     * @return 添加成功返回 true, 否则返回 false
     * @description: 向队列头部添加元素
     */
    @Override
    public boolean offerFirst(E e) {
        if (isFull()) {
            return false;//队列已经满了,直接return false添加失败
        }
        //a  added  b
        Node<E> a = sentinel;
        Node<E> b = sentinel.next;
        //双向链表有四个指针需要改变,创建节点的时候待添加的值e, e.pre e.next已经初始化
        Node<E> added = new Node<>(a, e, b);
        a.next = added;
        b.pre = added;

        size++;
        return true;
    }

    /**
     * @param e 待添加的值
     * @return 添加成功返回 true, 否则返回 false
     * @description: 向队列尾部添加元素
     */
    @Override
    public boolean offerLast(E e) {
        if (isFull()) {
            return false;//队列已经满了,直接return false添加失败
        }
        //a  added  b
        Node<E> a = sentinel.pre;
        Node<E> b = sentinel;
        //双向链表有四个指针需要改变,创建节点的时候待添加的值e, e.pre e.next已经初始化
        Node<E> added = new Node<>(a, e, b);
        a.next = added;
        b.pre = added;

        size++;
        return true;
    }

    /**
     * @description: 移除队列头部元素
     * @return: 返回已删除的值
     **/
    @Override
    public E pollFirst() {
        if (isEmpty()){
            return null;
        }

        // a  removed b
        //先找到删除节点上一个和下一个,直接把removed节点跳过就删除了
        Node<E> a = sentinel;
        Node<E> removed = sentinel.next;
        Node<E> b = removed.next;

        a.next = b; //把中间removed跳过了
        b.pre = a;
        size--;
        return removed.value;
    }

    /**
     * @description: 向队列尾部删除元素
     * @return: 返回已删除的值
     **/
    @Override
    public E pollLast() {
        if (isEmpty()){
            return null;
        }

        // a  removed b    要删除的是最后一个,removed是最后一个,说明b是哨兵
        //先找到删除节点上一个和下一个,直接把removed节点跳过就删除了
        Node<E> removed = sentinel.pre;
        Node<E> b = sentinel;
        Node<E> a = removed.pre;

        a.next = b; //把中间removed跳过了
        b.pre = a;
        size--;
        return removed.value;
    }

    @Override
    public E peekFirst() {

        return sentinel.next.value;
    }

    @Override
    public E peekLast() {
        return sentinel.pre.value;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean isFull() {
        return size == capacity;
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            Node<E> p = sentinel.next;

            @Override
            public boolean hasNext() {
                return p != sentinel;//结束条件
            }

            @Override
            public E next() {
                E value = p.value;
                p = p.next;
                return value;
            }
        };
    }


}