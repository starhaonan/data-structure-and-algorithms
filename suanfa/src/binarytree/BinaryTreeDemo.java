package binarytree;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author: nanlemme
 * @className: BinaryTreeDemo
 * @description:
 * @since: jdk11
 */
public class BinaryTreeDemo {
    public static void main(String[] args) {
        //先创建一颗二叉树
        BinaryTree tree = new BinaryTree();
        //创建需要的节点
        TreeNode root = new TreeNode(1);
        TreeNode node2 = new TreeNode(2);
        TreeNode node3 = new TreeNode(3);
        TreeNode node4 = new TreeNode(4);
        TreeNode node5 = new TreeNode(5);
        tree.root = root;
        root.left = node2;
        root.right = (node3);
        node3.right = (node4);
        node3.left = (node5);

        System.out.println("前序遍历");//1 2 3 5 4
        tree.preOrder();
        System.out.println("中序遍历");//2 1 5 3 4
        tree.middleOrder();
        System.out.println("后序遍历");//2 5 4 3 1
        tree.postOrder();
    }
}
//先创建Node节点

//定义一个BinaryTree 二叉树
class BinaryTree {
    public TreeNode root;//根节点

    //前序遍历
    public void preOrder() {
        if (this.root != null)
            this.root.preOrder();
        else
            System.out.println("当前二叉树为空");
    }

    //中序遍历
    public void middleOrder() {
        if (this.root != null)
            this.root.middleOrder();
        else
            System.out.println("当前二叉树为空");
    }

    //后序遍历
    public void postOrder() {
        if (this.root != null)
            this.root.postOrder();
        else
            System.out.println("当前二叉树为空");
    }
}