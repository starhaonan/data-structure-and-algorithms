package binarytree.binarysearchtree;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @author: nanlemme
 * @className: BSTTree
 * @description: Binary Search Tree 二叉搜索树
 * @since: jdk11
 */
@AllArgsConstructor
@NoArgsConstructor
public class BSTTree {
    //作为一个树,要有根节点
    BSTNode root;

    /**
     * @description: 查找关键字对应的值
     * @param: [key] 关键字
     * @return: java.lang.Object 关键字对应的值
     **/
    public Object get(int key) {
        return doGet(root, key);
    }

    private Object doGet(BSTNode node, int key) {
        //递归结束条件
        if (node == null) {
            return null;//没找到
        }
        if (key < node.key) {//待查找的值在左边
            return doGet(node.left, key);
        } else if (node.key < key) {//待查找的值在右边
            return doGet(node.right, key);
        } else {
            return node.value;//找到了,返回值
        }
    }

    /**
     * @description: 查找最小关键字对应值
     * @return: Object 关键字对应的值
     **/
    public Object min() {
        if (root == null) {
            return null;
        }
        BSTNode p = root;
        while (p.left != null) {
            p = p.left;
        }
        return p.value;
    }

    //递归实现
    public Object minRecurision(BSTNode node) {
        //递归实现
        if (node == null) { //没有一个节点
            return null;
        }
        if (node.left == null) {
            return node.value;
        }
        return minRecurision(node.left);
    }


    /**
     * @description: 查找最大关键字对应值
     * @return: java.lang.Object 关键字对应的值
     **/
    public Object max() {
        if (root == null)
            return null;
        BSTNode p = root;
        while (p.right != null) {
            p = p.right;
        }
        return p.value;
    }

    //传进的节点是什么,从传入节点开始找最大值
    public Object max(BSTNode node) {
        if (root == null)
            return null;
        node = root;
        while (node.right != null) {
            node = node.right;
        }
        return node.value;
    }

    //递归实现
    public Object maxRecurision(BSTNode node) {
        if (node == null)
            return null;
        if (node.right == null)
            return node.value;
        return maxRecurision(node.right);
    }

    /**
     * @description: 存储关键字和对应值
     * @param: [key, value] [关键字,值]
     **/
    public void put(int key, Object value) {
        //先根据key,看数中有没有此节点
        //1.有key的话,更新
        BSTNode node = root;
        BSTNode parsent = null;
        while (node != null) {
            parsent = node;
            if (key < node.key) {//待查找的值在树左边
                node = node.left;
            } else if (node.key < key) {//待查找的值在树右边
                node = node.right;
            } else { //找到了,更新一下value
                node.value = value;
                return;
            }
        }
        //如果树为空,那就新的节点作为根节点
        if (parsent == null) {
            root = new BSTNode(key, value);
            return;
        }

        //2.没有key的话,新增
        if (key < parsent.key) {
            parsent.left = new BSTNode(key, value);
        } else {
            parsent.right = new BSTNode(key, value);
        }

    }

    /**
     * @description: 查找关键字的前驱值
     * @param: [key] 关键字
     * @return: java.lang.Object  前驱值
     **/
    public Object predecessor(int key) {
        BSTNode ancestorFromLeft = null;
        BSTNode p = root;
        while (p != null) {
            if (key < p.key) { //待查找的值在左边
                p = p.left;
            } else if (p.key < key) {//待查找的值在右边
                ancestorFromLeft = p;//只要向右走,说明这个祖先向右而来
                p = p.right;
            } else {//相等,找到了
                break;
            }
        }

        if (p == null) {//没有找到节点
            return null;
        }
        // 情况1 - 有左孩子   节点有左子树,此时前任就是左子树的最大值
        if (p.left != null) {
            return max(p.left);
        }
        // 情况2 - 有祖先自左而来  节点没有左子树, 自左而来并且离他最近的一个祖先就是前任
        return ancestorFromLeft != null ? ancestorFromLeft.value : null;
    }

    /**
     * @description: 查找关键字的后继值
     * @param: [key] 关键字
     * @return: java.lang.Object 后继值
     **/
    public Object successor(int key) {
        return null;
    }

    /**
     * @description: 根据关键字删除
     * @param: [key] 关键字
     * @return: java.lang.Object
     **/
    public Object delete(int key) {

        return null;
    }
}
