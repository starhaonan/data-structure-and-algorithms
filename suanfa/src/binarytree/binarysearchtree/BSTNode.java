package binarytree.binarysearchtree;

import lombok.*;

/**
 * @author: nanlemme
 * @className: BSTNode
 * @description:
 * @since: jdk11
 */
@ToString
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class BSTNode {
    public int key;
    public Object value;
    public BSTNode left;
    public BSTNode right;

    public BSTNode(int key, Object value) {
        this.key = key;
        this.value = value;
    }
}
