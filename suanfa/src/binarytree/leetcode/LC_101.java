package binarytree.leetcode;


import binarytree.TreeNode;

/**
 * @author: nanlemme
 * @className: LC_101
 * @description:  求二叉树的最小深度
 * @since: jdk11
 */
public class LC_101 {
    public boolean isSymmetric(TreeNode root) {
        return check(root.left, root.right);
    }

    public boolean check(TreeNode left, TreeNode right) {
        // 若同时为 null
        if (left == null && right == null) {
            return true;
        }
        // 若有一个为 null (有上一轮筛选，另一个肯定不为 null)
        //到这一步说明,左右节点不能同时为空
        if (left == null || right == null) {
            return false;
        }
        if (left.val != right.val) {
            return false;
        }
        //左节点的右孩子和右节点左孩子,左节点的左孩子和右节点的右孩子对比,同时成立才说明对称
        //         1
        //       /    \
        //      2      2
        //     / \    / \
        //    3   4  4   3
        return check(left.left, right.right) && check(left.right, right.left);
    }
}
