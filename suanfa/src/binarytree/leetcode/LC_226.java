package binarytree.leetcode;

import binarytree.TreeNode;

/**
 * @author: nanlemme
 * @className: LC_226
 * @description:    反转二叉树
 * @since: jdk11
 */
public class LC_226 {
    public static void main(String[] args) {
        TreeNode root = new TreeNode(new TreeNode(2),1,null);
        LC_226.fn(root);
    }

    private static void fn(TreeNode node){
        if (node == null){
            return;
        }
        //二叉树左右交换
        TreeNode tempNode = node.left;
        node.left = node.right;
        node.right = tempNode;
        //递归调用左子树,右子树. 如果为空就不递归了
        fn(node.left);
        fn(node.right);
    }
}
