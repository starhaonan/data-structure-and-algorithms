package binarytree.leetcode;

import binarytree.TreeNode;

/**
 * @author: nanlemme
 * @className: LC_112
 * @description:  求二叉树最小深度(后序遍历求解)
 * @since: jdk11
 */
public class LC_111_1 {
    public int minDepth(TreeNode node) {
        if (node == null)
            return 0;
        int i = minDepth(node.left);
        int j = minDepth(node.right);
        //如果右子树为null时
        if (j == 0)
            return i + 1;
        //如果左子树为null时
        if (i == 0)
            return j + 1;
        return Integer.max(i, j);
    }
}
