package binarytree.redblacktree;

import static binarytree.redblacktree.redBlackTree.Color.BLACK;
import static binarytree.redblacktree.redBlackTree.Color.RED;

/**
 * @author: nanlemme
 * @className: redBlackTree
 * @description: 红黑树
 * @since: jdk11
 */
public class redBlackTree {
    enum Color {
        RED, BLACK;
    }

    private Node root;

    private static class Node {
        int key;
        Object value;
        Node left;
        Node right;
        Node parent;    //父节点
        Color color = RED; //默认插入节点为红色

        //是否左孩子
        boolean isLeftChild() {
            return parent != null && parent.left == this;
        }

        //叔叔
        Node uncle() {
            if (parent == null || parent.parent == null) {// 根节点为空 || 爷爷节点为空
                return null;  //一个节点没有父亲节点或者爷爷节点是没有叔叔节点的
            }
            if (parent.isLeftChild()) {//如果父亲是爷爷节点的左孩子
                return parent.parent.right; //叔叔节点就是爷爷的右孩子
            } else {//父节点是爷爷节点的右孩子
                return parent.parent.left;//叔叔节点就是爷爷的左孩子
            }
        }

        //兄弟
        Node sibling() {
            if (parent == null) {//没有父亲
                return null;
            }
            if (this.isLeftChild()) {//当前节点是父亲的左孩子
                return parent.right; //返回父亲的右孩子,也就是兄弟节点
            } else {//当前节点是父亲的右孩子
                return parent.left;//返回父亲的左孩子,也就是兄弟节点
            }
        }

    }

    //判断红
    boolean isRed(Node node) {
        //节点不等于空(第二条) && 节点颜色等于红色
        return node != null && node.color == RED;
    }

    //判断黑
    boolean isBlack(Node node) {
        //节点等于空(第二条) && 节点颜色等于黑色
        return node == null && node.color == BLACK;
    }

    //右旋
    //和AVL树不一样的地方在: 1.parent的处理  2.旋转后新根的父子关系
    private void rightRotate(Node node){

    }

    //左旋
    private void leftRotate(Node node){

    }
}
