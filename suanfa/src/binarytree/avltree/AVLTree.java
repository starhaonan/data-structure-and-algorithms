package binarytree.avltree;

/**
 * @author: nanlemme
 * @className: AVLTree
 * @description: AVL树
 *               二叉搜索树在插入和删除时, 节点可能失衡
 *               如果在插入和删除时通过旋转, 始终让二叉搜索树保持平衡, 称为自平衡的二叉搜索树
 *               AVL是自平衡二叉搜索树的实现之一
 */

public class AVLTree {

    static class AVLNode {
        int key;
        Object value;
        AVLNode left;
        AVLNode right;
        int height = 1;//高度

        public AVLNode(int key) {
            this.key = key;
        }

        public AVLNode(int key, Object value) {
            this.key = key;
            this.value = value;
        }

        public AVLNode(int key, Object value, AVLNode left, AVLNode right) {
            this.key = key;
            this.value = value;
            this.left = left;
            this.right = right;
        }
    }


    /**
     * @description: 求节点高度
     * @param: [node]  传进来的节点值
     * @return: int 树的高度
     **/
    private int height(AVLNode node) {
        return node == null ? 0 : node.height;
    }

    /**
     * @description: 更新节点高度(新增, 删除, 旋转)
     * @param: [node]  节点
     **/
    private void updateHeight(AVLNode node) {
        //求出来节点左孩子的高度,和右孩子的高度,求一个最大值加一.(用的是上面的height方法)
        //然后赋值给节点的height就更新成功了
        node.height = Integer.max(height(node.left), height(node.right)) + 1;
    }

    /**
     * @description: 平衡因 子(balance factor) = 左子树高度 - 右子树高度
     * @param: [node] 传入节点
     * @return: int  0 -1 1 平衡的     >1 or <-1不平衡  (两树高度相差1以上这棵树不是平衡的)
     **/
    private int bf(AVLNode node) {
        return height(node.left) - height(node.right);
    }

    /**
     * @description: 右旋
     * @param: [red]  要旋转的节点-->失衡的节点
     * @return: 新的根节点
     **/
    private AVLNode rightRotate(AVLNode red) {
        AVLNode yellow = red.left;
        //换爹----> 把2的右节点3 变成4的左节点
        red.left = yellow.right;
        //上位--->把红色变成黄色右节点(因为之前绿的爹换成红色了,也跟着红色分到右边了)
        yellow.right = red;
        //右旋成功,更新一下高度
        //先算上面的高度,再算下面的.不能反过来,因为下面还没计算出正确值,先统计上面的话会错
        updateHeight(red);
        updateHeight(yellow);
        return yellow; //返回根(旋转好的)节点
    }

    /**
     * @description: 左旋
     * @param: [red] 要旋转的节点-->失衡的节点
     * @return: 新的根节点
     **/
    private AVLNode leftRotate(AVLNode red) {
        AVLNode yellow = red.right;
        //换爹
        red.right = yellow.left;
        //上位
        yellow.left = red;
        //右旋成功,更新一下高度
        //先算上面的高度,再算下面的.不能反过来,因为下面还没计算出正确值,先统计上面的话会错
        updateHeight(red);
        updateHeight(yellow);
        return yellow;
    }

    /**
     * @description: 先左旋左子树, 再右旋根节点
     * @param: [root] 要旋转的节点-->失衡的节点
     * @return: 新的根节点
     **/
    private AVLNode leftRightRotate(AVLNode root) {
        //先把左子树左旋,调用之前写好的leftRotate()返回值为旋转好 子树的新根节点
        //把旋转好 子树的新根节点 赋值给node.left
        root.left = leftRotate(root.left);
        //右旋根节点
        return rightRotate(root);
    }

    /**
     * @description: 先右旋右子树, 再左旋根节点
     * @param: [root] 要旋转的节点-->失衡的节点
     * @return: 新的根节点
     **/
    private AVLNode rightLeftRotate(AVLNode root) {
        //先把右子树右旋,调用之前写好的rightRotate() 返回值为旋转好 子树的新根节点
        //把旋转好 子树的新根节点 赋值给node.right
        root.right = rightRotate(root.right);
        //左旋根节点
        return leftRotate(root);
    }

    /**
     * @return 平衡过的节点
     * @description: 检查节点是否失衡, 重新平衡代码
     * @param: [node]
     **/
    private AVLNode balance(AVLNode node) {
        if (node == null) {
            return null;
        }
        int bf = bf(node);
        if (bf > 1 && bf(node.left) >= 0) {   //LL
            return rightRotate(node);//右旋
        } else if (bf > 1 && bf(node.left) < 0) { //LR
            return leftRightRotate(node);//左右旋
        } else if (bf < -1 && bf(node.right) > 0) {//RL
            return rightLeftRotate(node);//右左旋
        } else if (bf < -1 && bf(node.right) <= 0) { //RR
            return leftRotate(node);//左旋
        }
        return node;
    }

    /**
     * @description:
     * @param: [root, key, value]  [起始节点, 新增的key, 新增的value]
     * @return: void
     **/
    AVLNode root;

    public void put(int key, Object value) {
        root = doPut(root, key, value);
    }

    private AVLNode doPut(AVLNode node, int key, Object value) {
        //1.找到空位, 创建新节点
        if (node == null) {
            return new AVLNode(key, value);
        }
        //2. key已存在, 更新
        if (key == node.key) {
            node.value = value;
            return node;
        }
        //3.继续查找(查找的节点小于根节点的值,放在左边)
        if (key < node.key) {
            node.left = doPut(node.left, key, value); //向左查找,并和根节点左建立关系
        } else {//查找的节点大于根节点的值,放在右边
            node.right = doPut(node.right, key, value);//向右查找,并和根节点右建立关系
        }
        //更新高度
        updateHeight(node);
        //检查是否失衡
        return balance(node);
    }

    public void remove(int key) {
        root = doRemove(root, key);
    }

    private AVLNode doRemove(AVLNode node, int key) {
        //1. node为空的情况
        if (node == null) {
            return null;
        }
        //2.没找到key
        if (key < node.key) {
            node.left = doRemove(node.left, key);
        } else if (node.key < key) {
            node.right = doRemove(node.right, key);
        } else {
            //3.找到key 1)没有孩子 2)只有一个孩子 3)有两个孩子
            if (node.left == null && node.right == null) {
                return null;
            } else if (node.left == null) {
                node = node.right;
            } else if (node.right == null) {
                node = node.left;
            } else {
                AVLNode s = node.right;
                while (s.left != null) {
                    s = s.left;
                }
                // s 后继节点
                s.right = doRemove(node.right, s.key);
                s.left = node.left;
                node = s;
            }
        }
        //4. 更新高度
        updateHeight(node);
        //5.balance
        return balance(node);
    }

}
