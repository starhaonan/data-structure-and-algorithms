package binarytree;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class TreeNode {
    public TreeNode left;
    public Integer val;
    public TreeNode right;

    public TreeNode(Integer val) {
        this.val = val;
    }

    //编写前序遍历的方法
    public void preOrder() {
        System.out.println(this);//先输出父节点
        //递归向左右子树遍历
        if (this.left != null)
            this.left.preOrder();
        if (this.right != null)
            this.right.preOrder();
    }

    //中序遍历的方法
    public void middleOrder() {
        //递归向左子树,中序遍历
        if (this.left != null)
            this.left.middleOrder();
        System.out.println(this);//输出父节点
        if (this.right != null)
            this.right.middleOrder();
    }

    //后序遍历
    public void postOrder() {
        //递归向左右子树,后序遍历
        if (this.left != null)
            this.left.postOrder();
        if (this.right != null)
            this.right.postOrder();
        System.out.println(this);
    }

    @Override
    public String toString() {
        return "Node{" +
                "val=" + val +
                '}';
    }
}
