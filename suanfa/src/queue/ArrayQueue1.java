package queue;

/**
 * @author: Star
 * @className: ArrayQueue1
 * @description:
 * @date: 2023/7/27 8:02
 * @version: 1.0
 * @since: jdk11
 */
public class ArrayQueue1<E> implements Queue<E> {
    private final E[] array;
    private int head = 0;
    private int tail = 0;
    private int size = 0; //统计数组数据的长度

    @SuppressWarnings("all")
    public ArrayQueue1(int capacity) {
        array = (E[]) new Object[capacity];
    }


    @Override
    public E peek() {
        if (isEmpty())
            return null;
        return array[head];
    }

    @Override
    public boolean offer(E value) {
        if (isEmpty())
            return false;
        array[tail] = value;
        tail = (tail + 1) % array.length;
        size++;
        return true;
    }

    @Override
    public E poll() {
        if (isEmpty())
            return null;
        E value = array[head];
        head = (head + 1) % array.length;
        size--;
        return value;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean isFull() {
        return size == array.length;
    }
}
