package queue;

/**
 * @author: Star
 * @className: ArrayQueue3
 * @description:
 * @date: 2023/7/28 8:27
 * @version: 1.0
 * @since: jdk11
 */
public class ArrayQueue3<E> implements Queue<E>{
    private int head = 0;
    private int tail = 0;
    private final E[] array;
    private final int capacity;
    @SuppressWarnings("all")
    public ArrayQueue3(int capacity) {
        if ((capacity & capacity - 1) != 0) {
            throw new IllegalArgumentException("capacity 必须为 2 的幂");
        }
        this.capacity = capacity;
        array = (E[]) new Object[this.capacity];
    }
    @Override
    public boolean offer(E value) {
        if (isFull())
            return false;
        array[tail % array.length] = value;
        tail++;
        return true;
    }
    @Override
    public E poll() {
        if (isEmpty())
            return null;
        E value = array[tail % array.length];
        head++;
        return value;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean isFull() {
        return false;
    }

    @Override
    public E peek() {
        if (isEmpty()) return null;
        return array[tail % array.length];
    }
}
