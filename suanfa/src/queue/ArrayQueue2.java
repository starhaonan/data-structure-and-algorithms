package queue;

/**
 * @author: Star
 * @className: ArrayQueue2
 * @description:
 * @date: 2023/7/27 9:02
 * @version: 1.0
 * @since: jdk11
 */

public class ArrayQueue2<E> implements Queue<E> {
    //仅用head, tail判断空满,head tail即为索引值.

    private final E[] array;
    private int head = 0;
    private int tail = 0;
    @SuppressWarnings("all")
    public ArrayQueue2(int capacity) {
        array = (E[]) new Object[capacity + 1];
    }
    @Override
    public E peek() {
        if (isEmpty())
            return null;
        return array[head];
    }

    @Override
    public boolean offer(E value) {
        if (isEmpty())
            return false;
        array[tail] = value;
        tail = (tail + 1) % array.length;

        return true;
    }

    @Override
    public E poll() {
        if (isEmpty())
            return null;
        E value = array[head];
        head = (head + 1) % array.length;
        return value;
    }

    @Override
    public boolean isEmpty() {
        return head == tail;
    }

    @Override
    public boolean isFull() {
        return head == (tail + 1) % 5;
    }

}
