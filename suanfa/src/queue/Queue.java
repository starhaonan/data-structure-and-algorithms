package queue;

/**
 * @author: Star
 * @className: Queue
 * @description:
 * @date: 2023/7/27 8:36
 * @version: 1.0
 * @since: jdk11
 */
public interface Queue<E> {


    /**
     * @description 往队列尾部添加数据
     * @param value 待插入的值
     * @return 插入成功返回true 失败false
     **/
    public abstract boolean offer(E value);

    /**
     * @description: 从队列的头部获取值,并移除
     * @return: E 如果队列非空返回对头值,否则返回null
     **/
    public abstract E poll();

    /**
     * @description: 从队列头部获取值,不删除
     * @return: E 如果队列头非空返回队列头值,为空返回null
     **/
    public abstract E peek();

    /**
     * @description: 判断队列是否为空
     * @param: []
     * @return: 空 返回一个true,否则返回false
     **/
    public abstract boolean isEmpty();

    /**
     * @description: 判断队列是否已满
     * @return: 满了返回true 不满返回false
     **/
    public abstract boolean isFull();

}
