package queue;

import lombok.AllArgsConstructor;

import java.util.Iterator;

/**
 * @author: nanlemme
 * @className: LinkedListQueue
 * @description: 用链表来实现队列: 从头出,队尾加
 * @since: jdk11
 */
public class LinkedListQueue<E> implements Queue<E>, Iterable<E> {

    /**
     * 静态内部类  节点类
     * 因为是单向所以只需要next指针
     **/
    @AllArgsConstructor
    private static class Node<E> {
        E value; //值
        Node<E> next;
    }

    //单向环形带哨兵链表  没有值的时候的状态(初始状态)
    //刚开始head头结点/tail尾结点 都指向哨兵节点
    Node<E> head = new Node<>(null, null);//哨兵节点的value是随意的,刚开始哨兵没办法指向自己,所以null
    Node<E> tail = head;
    private int size = 0;//节点数(往队列加了几个节点)

    //队列容量(在队列创建指定队列的长度),默认为无限.-->在创建调用带参构造创建对象时可以指定,以指定为准
    private int capacity = Integer.MAX_VALUE;

    public LinkedListQueue(int capacity) {
        this.capacity = capacity;
    }
    public LinkedListQueue() {
        tail.next = head; //把链表变成环形
    }

    /**
     * @param value 待插入的值
     * @return 插入成功返回true 失败false
     * @description 往队列尾部添加数据
     **/
    @Override
    public boolean offer(E value) {
        if (isFull()){
            return false;//队列满了,返回false添加失败
        }
        Node<E> added = new Node<>(value, head); //新加入的节点,next只想head
        tail.next = added; //因为tail现在还指向head,所以更新tail
        tail = added;//让新节点变成尾结点
        size++;//队列尾部添加成功,节点数量+1
        return true;
    }

    /**
     * @description: 从队列的头部获取值, 并移除
     * @return: E 如果队列非空返回对头值,否则返回null
     **/
    @Override
    public E poll() {
        if (isEmpty()) {//队列为空
            return null;
        }
        //先把第一个值取出来
        Node<E> first = head.next;
        //删除第一个值
        head.next = first.next;
        //如果节点中只有一个值的话(tail指向的值就不对了)
        if (first == tail) {//第一个节点是尾结点,也就是队列中只有一个值
            tail = head;
        }
        size--;//队列尾部删除成功,节点数-1
        return first.value;
    }


    /**
     * @description: 从队列头部获取值, 不删除
     * @return: E 如果队列头非空返回队列头值,为空返回null
     **/
    @Override
    public E peek() {
        if (isEmpty()) {//队列为空
            return null;
        }
        return head.next.value;
    }

    /**
     * @description: 判断队列是否为空
     * @return: 空 返回一个true,否则返回false
     **/
    @Override
    public boolean isEmpty() {
        //head和tail相等 说明队列没有值 队列为空,否则有值head和tail不等,不为空
        return head == tail;
    }

    /**
     * @description: 判断队列是否已满
     * @return: 满了返回true 不满返回false
     **/
    @Override
    public boolean isFull() {
        return capacity == size;
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            //声明一个指针, 哨兵节点不用做迭代,所以从head.next开始
            Node<E> p = head.next;

            @Override
            public boolean hasNext() {
                return p != head;
            }

            @Override
            public E next() {
                E value = p.value;
                p = p.next;//每次走一步
                return value;
            }
        };
    }

}
