package sort;

import java.util.Arrays;

/**
 * @author: Star
 * @className: SelectSort
 * @description: 选择排序
 * @date: 2023/7/25 8:15
 * @version: 1.0
 * @since: jdk11
 */
public class SelectSort {
    public static void main(String[] args) {

        int[] nums = {101, 29, 23,1};
        selectSort(nums);
    }

    public static void selectSort(int[] nums) {
        for (int i = 0; i < nums.length - 1; i++) {
            //1.假定第一个数为最小值
            int minIndex = i;
            int min = nums[i];
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[j] < min) {
                    min = nums[j];//重置最小值
                    minIndex = j;//重置最小值的最小索引
                }
            }
            nums[minIndex] = nums[i];
            nums[i] = min;
        }
        System.out.println(Arrays.toString(nums));
    }
}
