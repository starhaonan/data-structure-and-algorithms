package priorityqueue;

/**
 * @author: nanlemme
 * @className: Entry
 * @description:  用来配合PriorityQueueTest,测试
 * @since: jdk11
 */
public class Entry implements Priority{
    String value;
    int priority;

    public Entry(String value, int priority) {
        this.value = value;
        this.priority = priority;
    }

    @Override
    public int priority() {
        return priority;
    }

    @Override
    public String toString() {
        return "Entry{" +
                "value='" + value + '\'' +
                ", priority=" + priority +
                '}';
    }
}
