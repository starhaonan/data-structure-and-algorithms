package priorityqueue;

/**
 * @author: nanlemme
 * @className: Priority
 * @description:
 * @since: jdk11
 */
public interface Priority {

    /**
     * @description: 返回对象的优先级, 约定:数字越大优先级越高
     * @return: 优先级
     **/
    public abstract int priority();
}
