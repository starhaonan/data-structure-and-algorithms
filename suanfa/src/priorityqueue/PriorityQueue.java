package priorityqueue;

import queue.Queue;

/**
 * @author: nanlemme
 * @className: PriorityQueue
 * @description: 优先级队列 -->基于数组实现
 * @since: jdk11
 */
public class PriorityQueue<E extends Priority> implements Queue<E> {

    Priority[] array;
    int size;

    public PriorityQueue(int capacity) {
        array = new Priority[capacity];
    }

    /**
     * @description: 从队列尾部添加数据
     * @param: 待添加的元素
     * @return: true添加成功  false添加失败
     **/
    @Override
    public boolean offer(E e) {
        if (isFull()) {
            return false;
        }
        array[size] = e;
        size++;
        return true;
    }

    /**
     * @return: 返回优先级最高的元素的索引值
     **/
    private int selectMax() {
        int max = 0;
        for (int i = 1; i < size; i++) {//默认max所在的0索引是最大值,所以从1索引开始遍历
            if (array[i].priority() > array[max].priority()) {
                max = i;  //有更大的值,更新一下
            }
        }
        return max;
    }

    /**
     * @description: 根据索引删除元素
     * @param: 待删除的元素索引
     **/
    private void remove(int index) {
        if (index < size - 1) {
            //移动
            System.arraycopy(array, index + 1, array, index, size - 1);
        }
        size--;
    }

    /**
     * @description: 删除队列中优先级最高的元素-->出队
     * @return: 返回删除的元素
     **/
    @Override
    public E poll() {
        if (isEmpty()) {
            return null;
        }
        int max = selectMax();//优先级最高的元素的索引值
        E e = (E) array[max];//优先级最高的元素
        remove(max);
        return e;
    }

    /**
     * @return: 返回队列中优先级最高的元素
     **/
    @Override
    public E peek() {
        if (isEmpty()) {
            return null;
        }
        int max = selectMax();
        return (E) array[max];
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean isFull() {
        return size == array.length;
    }
}
