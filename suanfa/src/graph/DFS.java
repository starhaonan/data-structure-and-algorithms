package graph;

import java.util.LinkedList;
import java.util.List;

/**
 * @author: nanlemme
 * @className: DFS
 * @description: 深度优先搜素 Depth-first- search
 *                两种实现方式   递归 / 栈
 * @since: jdk11
 */
public class DFS {
    public static void main(String[] args) {
        Vertex v1 = new Vertex("v1");
        Vertex v2 = new Vertex("v2");
        Vertex v3 = new Vertex("v3");
        Vertex v4 = new Vertex("v4");
        Vertex v5 = new Vertex("v5");
        Vertex v6 = new Vertex("v6");

        v1.edges = List.of(new Edge(v3), new Edge(v2), new Edge(v6));
        v2.edges = List.of(new Edge(v4));
        v3.edges = List.of(new Edge(v4), new Edge(v6));
        v4.edges = List.of(new Edge(v5));
        v5.edges = List.of();
        v6.edges = List.of(new Edge(v5));

        dfs2(v1);  //用栈模拟
    }

    private static void dfs2(Vertex v) {
        LinkedList<Vertex> stack = new LinkedList<>();
        //先把根顶点 加入栈中
        stack.push(v);
        //如果栈不为空,一直遍历
        while (!stack.isEmpty()) {
            //每次遍历的时候出栈一个元素
            Vertex pop = stack.pop();
            //拿出来这个元素相当于访问过了,把visited置为false
            pop.visited = true;
            System.out.println(pop.name);
            for (Edge edge : pop.edges) {//循环拿到pop相邻的顶点
                //如果edge相邻的节点没有被访问过(visited=false),把它压入栈
                if (!edge.linked.visited) {
                    stack.push(edge.linked);
                }
            }
        }
    }

    //递归方式
    private static void dfs(Vertex v) {
        v.visited = true; //顶点进来,代表此顶点别访问过了(打印一下名字,代表被访问了)
        System.out.println(v.name);

        //对edge进行遍历
        for (Edge edge : v.edges) {
            //每条edge都有一个顶点
            if (!edge.linked.visited) {//如果当前节点没有被访问过
                dfs(edge.linked); //对当前没有别访问过的节点,再进行递归
            }
        }
    }

}
