package graph;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: nanlemme
 * @className: Vertex
 * @description: 顶点
 * @since: jdk11
 */
public class Vertex {
    String name;
    boolean visited; //当前节点有没有被访问过
    List<Edge> edges;

    public Vertex(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }


  /*  public static void main(String[] args) {
        *//*
                     A
                    /  \
                   B    C
                    \  /
                      D
         **//*
        Vertex a = new Vertex("A");
        Vertex b = new Vertex("B");
        Vertex c = new Vertex("C");
        Vertex d = new Vertex("D");
        a.edges = List.of(new Edge(b),new Edge(c));
        b.edges = List.of(new Edge(d));
        c.edges = List.of(new Edge(d));
        d.edges = List.of();
    }*/
}
