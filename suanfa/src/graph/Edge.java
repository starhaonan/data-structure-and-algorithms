package graph;

/**
 * @author: nanlemme
 * @className: Edge
 * @description:  边
 * @since: jdk11
 */
public class Edge {
    Vertex linked; //边链接的顶点
    int weight; //权重

    public Edge(Vertex linked){
        //调用另一个全参构造
        this(linked, 1);//权重默认为1
    }

    public Edge(Vertex linked, int weight){
        this.linked = linked;
        this.weight = weight;
    }
}
