package linkedlist;

/**
 * @author: Star
 * @className: test.SinglyLinkedList
 * @description:
 * @date: 2023/7/11 8:44
 * @version: 1.0
 * @since: jdk11
 */

class SinglyLinkedListDemo{
    public static void main(String[] args) {
        Node node1 = new Node(1);
        Node node2 = new Node(2);
        Node node3 = new Node(3);
        Node node4 = new Node(4);
        Node node5 = new Node(5);

        //创建链表
        SinglyLinkedList singlyLinkedList = new SinglyLinkedList();
        singlyLinkedList.add(node1);
        singlyLinkedList.add(node4);
        singlyLinkedList.add(node2);
        singlyLinkedList.add(node5);
        singlyLinkedList.add(node3);
      /*  System.out.println("------------------");
        singlyLinkedList.addByOrder(node1);
        singlyLinkedList.addByOrder(node4);
        singlyLinkedList.addByOrder(node2);
        singlyLinkedList.addByOrder(node5);
        singlyLinkedList.addByOrder(node3);*/


        singlyLinkedList.list();

    }
}
class SinglyLinkedList {
    //初始化一个头结点,头结点不要动,不存放具体的数据
    private Node  head = new Node(0);

    //遍历打印列表
    public void list(){
        //判断链表是否为空
        if (head.next == null){
            System.out.println("链表为空");
            return;
        }
        Node temp = head.next;
        while (true){
            //判断是否到链表最后
            if (temp == null){
                break;
            }
            System.out.println(temp);
            temp = temp.next;
        }
    }
    public  void add(Node node){
        //因为头结点不能动,所以需要一个辅助节点
        Node temp = head;
        while (true){
            if (temp.next == null) break;//链表后面没值了


            //如果没找到,将temp后移一位
            temp = temp.next;
        }
        //当退出while循环时,代表temp就是最后一个值
        temp.next = node;
    }

    //通过id添加节点
    public void addByOrder(Node node){
        Node temp = head;
        boolean flag = false;
        while (true){
            if (temp.next == null){
                break;
            }
            if (temp.next.value > node.value){//位置找到,就在temp后面插入
                break;
            } else if (temp.next.value == node.value) {//说明节点已经存在
                flag = true;//说明编号存在
                break;
            }
            temp = temp.next;
        }
        if (flag ){
            System.out.println("准备插入的节点已经存在");
        }else {
            node.next = temp.next;
            temp.next = node;

        }
    }
}
class Node {
    int value;//值
    Node next;//下一个节点指针

    public Node(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Node{" +
                "value=" + value +
                '}';
    }
}