package linkedlist;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author: Star
 * @className: JosepFu
 * @description:
 * @date: 2023/7/16 15:55
 * @version: 1.0
 * @since: jdk11
 */
public class JosepFu {
    public static void main(String[] args) {
        SingleCircleLinkedList singleCircleLinkedList = new SingleCircleLinkedList();
        singleCircleLinkedList.addBoy(25);
        singleCircleLinkedList.showBoy();
    }
}

//创建一个单向的环形链表
class SingleCircleLinkedList {

    //创建一个fist节点,当前没有编号
    private Boy firstBoy = new Boy(-1);

    /**
     * @description: 添加小孩节点, 构建一个环形链表
     * @param: [nums] 环形链表有几个节点
     **/
    public  void addBoy(int nums) {
        if (nums < 1) {
            System.out.println("nums的值不合法");
            return;
        }
        Boy currBoy = null;//辅助指针,帮我构建环形链表
        //使用for循环,创建环形链表
        for (int i = 1; i <= nums; i++) {
            Boy boy = new Boy(i);//根据编号,创建小孩节点
            if (i == 1) {
                firstBoy = boy;
                firstBoy.setNext(firstBoy);//构成环
                currBoy = firstBoy;//让currBoy指向第一个小孩
            } else {
                currBoy.setNext(boy);
                boy.setNext(firstBoy);
               // currBoy = boy;
            }
        }
    }

    //遍历当前的链表
    public void showBoy(){
        //判断链表是否为空
        if (firstBoy == null){
            System.out.println("没有任何小孩");
            return;
        }
        //因为firstBoy不能动,所以创建一个辅助指针,来遍历
        Boy currBoy = firstBoy;
        while (true){
            System.out.printf("小孩的编号为:%d \n",currBoy.getNo());
            if (currBoy.getNext() == firstBoy){//说明已经遍历完毕
                 break;
            }
            currBoy = currBoy.getNext();//currBoy后移
        }
    }
}
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
class Boy {
    private int no;
    private Boy next;//指向下一个结点,默认为null
    public Boy(int no) {
        this.no = no;
    }
}
