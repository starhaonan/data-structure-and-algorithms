package leetcode.array;

import java.util.Arrays;

/**
 * @author: Star
 * @className: Demo724_pivotIndex
 * @description:
 * @date: 2023/7/5 11:38
 * @version: 1.0
 * @since: jdk11
 */
public class Demo724_pivotIndex {
    public static void main(String[] args) {
        int[] nums = {1, 7, 3, 6, 5, 6};
                        //     3  11
        System.out.println(pivotIndex(nums));

    }
    public static int pivotIndex(int[] nums) {
        int leftSum = 0;
        int rightSum = Arrays.stream(nums).sum();

        for (int i = 0; i < nums.length - 1; i++) {

            rightSum = rightSum - nums[i];
            if (rightSum == leftSum) {
                return i;
            }
            leftSum = leftSum + nums[i];
        }
        return -1;
    }
}
