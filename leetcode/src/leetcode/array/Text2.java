package leetcode.array;

/**
 * @author: Star
 * @className: Text2
 * @description:
 * @date: 2023/7/22 9:55
 * @version: 1.0
 * @since: jdk11
 */
public class Text2 {
    public static void main(String[] args) {
        int[] nums = {2,7,1,19,18,3};
        System.out.println(sumOfSquares(nums));

    }


    public static int sumOfSquares(int[] nums) {
        int sum = 0;
        for (int i = 1; i <= nums.length; i++) {
            if (nums.length % i == 0){
                sum = sum + nums[i-1]*nums[i-1];
            }
        }
        return sum;
    }
}
