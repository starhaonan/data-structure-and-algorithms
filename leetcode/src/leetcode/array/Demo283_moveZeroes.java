package leetcode.array;

import java.util.Arrays;

/**
 * @author: Star
 * @className: Demo283
 * @description: moveZeroes
 * @date: 2023/7/3 18:48
 * @version: 1.0
 * @since: jdk11
 */
public class Demo283_moveZeroes {
    public static void main(String[] args) {
        int[] nums = {2, 0, 4, 0, 2};
        moveZeroes(nums);

    }

    public static void moveZeroes(int[] nums) {
        /*if(nums==null) {
            return;
        }
       int j = 0;
        for (int i = 0; i < nums.length; i++) {
            //当前元素不等于0，就把他交换到左边
            if (nums[i] != 0){
                    int temp = nums[i];
                    nums[i] = nums[j];
                    nums[j++] = temp;
            }

        }
        System.out.println(Arrays.toString(nums));*/

        //快慢指针

        int slow = 0;
        for (int fast = 0; fast < nums.length; fast++) {
            if (nums[fast] != 0) {
                nums[slow++] = nums[fast];
            }
        }
        while (slow < nums.length){
            nums[slow++] = 0;
        }
            System.out.println(Arrays.toString(nums));

    }
}
