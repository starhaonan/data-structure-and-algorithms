package leetcode.array;

import java.util.Arrays;

/**
 * @author: Star
 * @className: Demo704_search
 * @description:
 * @date: 2023/7/4 15:17
 * @version: 1.0
 * @since: jdk11
 */
public class Demo704_search {
    public static void main(String[] args) {
        int[] nums = {-1, 0, 3, 5, 9};
        System.out.println(search(nums, 3));
    }
    public static int search(int[] nums, int target) {
        int left = 0;
        int right = nums.length;
        while (left < right) {
            int middle = (right + left) / 2;
            if (nums[middle] < target) {
                left = middle + 1;
            } else if (target < nums[middle]) {
                right = middle ;
            }else {
                return middle;
            }
        }
        return -1;
    }
}
