package leetcode.array;

import java.util.Arrays;

/**
 * @author: Star
 * @className: Demo26_removeDuplicates
 * @description:
 * @date: 2023/7/4 8:38
 * @version: 1.0
 * @since: jdk11
 */
public class Demo26_removeDuplicates {
    public static void main(String[] args) {
        int[] nums = {0, 0, 1, 1, 1, 2, 2, 3, 3, 4};
         //             0, 1, 1, 1, 1, 2, 2, 3, 3, 4   1
          //            0, 1, 2, 3, 4, 2, 2, 3, 3, 4   2
        removeDuplicates(nums);

    }

    public static int removeDuplicates(int[] nums) {
        //快慢指针
        int slow = 0;
        int fast;
        for (fast = 0; fast < nums.length; fast++) {
            if (nums[slow] != nums[fast]) {
                nums[slow + 1] = nums[fast];
                slow++;
            }
        }
        System.out.println(Arrays.toString(nums));
        return  slow +1;
    }

}

