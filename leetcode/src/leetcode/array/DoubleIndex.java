package leetcode.array;

import java.util.Arrays;

/**
 * @author: Star
 * @className: DoubleIndex
 * @description: 编写一个函数，其作用是将输入的字符串反转过来。输入字符串以字符数组 s 的形式给出。
 * 不             要给另外的数组分配额外的空间，你必须原地修改输入数组、使用 O(1) 的额外空间解决这一问题。
 * @version: 1.0
 * @since: jdk11
 */
public class DoubleIndex {
    public static void main(String[] args) {
        int[] nums = {6, 2, 6, 5, 1, 2};
        System.out.println(reverseString(nums));
    }

    public static int reverseString(int[] nums) {
       /* int doubleSum = 0;
        int slow = 0;
        for (int fast = 0; fast < nums.length; fast++) {
            if (fast != slow){
                 doubleSum = nums[slow] + nums[fast];

            }*/
        Arrays.sort(nums);
        int result = 0;
        for (int i = 0; i < nums.length; i++) {
            if (i % 2 == 0)
                result = result + nums[i];
        }
        return result;
    }


}


