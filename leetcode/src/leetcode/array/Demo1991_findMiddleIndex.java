package leetcode.array;

import java.util.Arrays;

/**
 * @author: Star
 * @className: Demo1991_findMiddleIndex
 * @description:
 * @date: 2023/7/5 14:18
 * @version: 1.0
 * @since: jdk11
 */
public class Demo1991_findMiddleIndex {
    public static void main(String[] args) {
        int[] nums = {2, 3, -1, 8, 4};
        System.out.println(pivotIndex(nums));
    }
    public static int pivotIndex(int[] nums) {
        int sum = 0,left = 0,right =0;
        for (int i = 0; i < nums.length; i++) {
           sum= nums[i] + sum;
        }

        sum = Arrays.stream(nums).sum();
        if (sum - nums[0] == 0) {//如果数组中只有一个数据
            return 0;
        }

        //2, 3, -1, 8, 4
        for (int i = 0; i < nums.length; i++) {//left为数组中从索引为0的位置到索引为i的位置的所有元素的和
            left = left + nums[i];
            if (i != 0) {
                right = left - nums[i];
            }
            while (right == sum - left) {//总和减去左边的和右边的相等,就找到了
                return i;
            }
        }
        return -1;
    }

}