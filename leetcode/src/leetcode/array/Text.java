package leetcode.array;

import java.util.Arrays;

/**
 * @author: Star
 * @className: Text
 * @description:
 * @date: 2023/7/3 22:17
 * @version: 1.0
 * @since: jdk11
 */
public class Text {

    public static void main(String[] args) {
        int[] nums = new int[4];

        nums[0] = 11;
        nums[1] = 22;
        nums[2] = 33;
        nums[3] = 44;


        checkAndGrow(nums);


    }

    private static void checkAndGrow(int[] nums) {
        //容量检查:如果数组长度为0,赋一个初始空间8.否则,如果数组逻辑大小,也就是用过的等于数组长度,那就扩容1.5倍
        /*if (nums.length == 0){
            nums = new int[8];
        }else if (数组已使用的空间 == nums.length ) {*/
            //数组容量扩容1.5
            int length = nums.length;
            length += length >> 1;
            int[] newNums = new int[length];
            System.arraycopy(nums, 0, newNums, 0, nums.length);//把老数组,复制到新数组里面
            nums = newNums;//复制完了,旧数组没用了,把新数组地址值,指向旧数组.
            System.out.println(Arrays.toString(newNums));
            System.out.println(Arrays.toString(nums));
        }
}
