package leetcode.array;

/**
 * @author: Star
 * @className: Demo35_searchInsert
 * @description:
 * @date: 2023/7/4 19:21
 * @version: 1.0
 * @since: jdk11
 */
public class Demo35_searchInsert {
    public static void main(String[] args) {
        int[] nums = {-1, 0, 3, 5, 9};
        System.out.println(searchInsert(nums, 3));
    }
    public static int searchInsert(int[] nums, int target) {
        int right = nums.length;
        int left = 0;
        int middle = (left + right) >> 1;
        while (left <= right){
            if (target < nums[middle]){
                right = middle - 1;
            }else if (nums[middle] < target){
                left = left + 1;
            }else {
                return middle;
            }
        }
        return -1;
    }
}
