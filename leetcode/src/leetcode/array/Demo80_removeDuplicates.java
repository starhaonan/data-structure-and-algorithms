package leetcode.array;

import java.util.Arrays;

/**
 * @author: Star
 * @className: Demo80_removeDuplicates
 * @description:
 * @date: 2023/7/4 9:13
 * @version: 1.0
 * @since: jdk11
 */
public class Demo80_removeDuplicates {
    public static void main(String[] args) {
        int[] nums = {1, 2, 2, 3, 3, 3, 5, 5, 5, 5, 5};
        int[] onums = {1, 2, 2, 2, 3, 3, 3, 5, 5, 5, 5, 5};

        System.out.println(Arrays.toString(onums));

        removeDuplicates(nums);
        System.out.println(Arrays.toString(nums));


    }

    public static int removeDuplicates(int[] nums) {
        int index = 0;
        for (int temp : nums) {
            if (index < 2 || nums[index - 2] != temp)
                nums[index++] = temp;
        }
        return index;
    }


}
