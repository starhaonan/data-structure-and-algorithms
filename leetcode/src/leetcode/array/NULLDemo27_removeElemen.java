package leetcode.array;

import java.util.Arrays;

/**
 * @author: Star
 * @className: Demo27_removeElement
 * @description:
 * @date: 2023/7/3 20:43
 * @version: 1.0
 * @since: jdk11
 */
public class NULLDemo27_removeElemen {
    public static void main(String[] args) {
        int[] nums = {3, 2, 2, 3};
        System.out.println(removeElement(nums, 3));
        System.out.println(Arrays.toString(nums));


    }

    public static int removeElement(int[] nums, int val) {
        int n = nums.length;
        int left = 0;
        int right = nums.length;
        while (left < right) {
            if (nums[left] == val) {
                nums[left] = nums[right - 1];
                right--;
            } else {
                left++;
            }
        }
        return left;
    }




}
