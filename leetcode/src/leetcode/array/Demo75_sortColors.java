package leetcode.array;

import java.util.Arrays;

/**
 * @author: Star
 * @className: Demo75SortColors
 * @description: sortColors
 * @date: 2023/7/3 19:32
 * @version: 1.0
 * @since: jdk11
 */
public class Demo75_sortColors {
    public static void main(String[] args) {
        int[] nums = {2, 0, 2, 1, 1, 0};
        sortColors(nums);
        System.out.println(Arrays.toString(nums));
    }

    private static void sortColors(int[] nums) {

        for (int i = 1; i < nums.length; i++) {
            for (int j = 0; j < i; j++) {
                if (nums[j] > nums[i]) {
                    int temp = nums[i];
                    nums[i] = nums[j];
                    nums[j] = temp;
                }
            }
        }

    }

}
