package leetcode.array;

import java.util.Arrays;

/**
 * @author: Star
 * @className: Demo977_sortedSquares
 * @description:
 * @date: 2023/7/4 20:47
 * @version: 1.0
 * @since: jdk11
 */
public class Demo977_sortedSquares {
    public static void main(String[] args) {
        int[] nums = {-4,-1,0,3,10};
        int[] nums2 = {40,-1,0,3,10};

        sortedSquares(nums);
    }
    public static int[] sortedSquares(int[] nums) {
        int left = 0;
        // 右指针，指向原数组最右边
        int right = nums.length - 1;
        // 创建一个新数组，存储平方值
        int[] newNums = new int[nums.length];
        // 得到元素值平方值，从新数组最后位置开始写
        int newIndex = nums.length - 1;
        // 左右指针相遇（逐渐靠拢的过程）之后不再循环
        while (left <= right){
            // 如果原数组的左指针对应的平方值大于右指针，那么往新数组最后位置写入左指针对应的平方值
            if (Math.pow(nums[left], 2) > Math.pow(nums[right], 2)){
                newNums[newIndex] = (int) Math.pow(nums[left], 2);
                // 左指针右移
                left ++;
                // 移动新数组待写入的位置
                newIndex --;
            }
            else {
                newNums[newIndex] = (int) Math.pow(nums[right], 2);
                right --;
                newIndex --;
            }
        }
        System.out.println(Arrays.toString(newNums));
        return newNums;

    }
}
