package leetcode.binarytree;

/**
 * @author: nanlemme
 * @className: Demo101_isSymmetric
 * @description: 对称二叉树
 * @since: jdk11
 */
public class Demo101_isSymmetric {
    public boolean isSymmetric(TreeNode root) {
        return panduan(root.left, root.right);
    }

    public boolean panduan(TreeNode left, TreeNode right) {
        if (left == null && right == null) {
            return true;
        }
        if (left == null || right == null) {
            return false;
        }
        if (left.val != right.val) {
            return false;
        }
        return panduan(left.left, right.right) && panduan(left.right, right.left);
    }

}

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode() {
    }

    TreeNode(int val) {
        this.val = val;
    }

    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}