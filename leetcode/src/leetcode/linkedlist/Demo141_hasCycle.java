package leetcode.linkedlist;

import java.util.List;

/**
 * @author: Star
 * @className: Demo141_hasCycle
 * @description:
 * @date: 2023/7/16 17:36
 * @version: 1.0
 * @since: jdk11
 */
public class Demo141_hasCycle {
    public static void main(String[] args) {


    }

    public static boolean  hasCycle(ListNode head) {
        ListNode fast = head;
        ListNode slow = head;
        while (fast != null && fast.next != null){
            fast = fast.next.next;
            slow = slow.next;
            if (fast == slow){
                return true;
            }
        }
        return false;
    }
}

class ListNode {
    int val;
    ListNode next;

    ListNode(int x) {
        val = x;
        next = null;
    }
}