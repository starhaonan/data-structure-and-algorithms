package leetcode.linkedlist;

/**
 * @author: Star
 * @className: Demo206_reverseList
 * @description:
 * @date: 2023/7/16 17:24
 * @version: 1.0
 * @since: jdk11
 */

public class TODODemo206_reverseList {
    public ListNode reverseList(ListNode head) {


        return null;

    }

    public class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }
}