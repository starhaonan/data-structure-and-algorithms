package leetcode;

/**
 * @author: Star
 * @className: Demo35
 * @description: 搜索插入位置(可以使用二分查找)
 * @date: 2023/7/2 16:54
 * @version: 1.0
 * @since: jdk11
 */
public class Demo35 {

    public static void main(String[] args) {
        int[] nums = {1, 3, 5, 6};
        int target = 2;

        System.out.println(searchInsert(nums,target));
    }

    public static int searchInsert(int[] nums, int target) {
        if (nums == null) {
            return -1;
        }
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] >= target) {
                return i;
            }
        }
        return nums.length;
    }


}
